Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  root "shorturls#new"
  resources :shorturls  
  get "/:short_url", to: "shorturls#load"
end
