class Shorturl < ActiveRecord::Base
    validates :raw_url, presence: true 
end