class ShorturlsController < ApplicationController
    def new
        @url = Shorturl.new
    end
    def create
        # render plain:params[:shorturl].inspect
        @url = Shorturl.new(url_params)
        @url.clean_url = @url.raw_url.strip.downcase.gsub(/(https?:\/\/)|(www\.)/, "")
        @url.clean_url.slice!(-1) if @url.clean_url[-1] == "/"
        @url.clean_url = "http://#{@url.clean_url}"
        @url.short_url = [*('A'..'Z'),*('0'..'9'),*('a'..'z')].sample(6).join
        while Shorturl.find_by_short_url([*('A'..'Z'),*('0'..'9'),*('a'..'z')].sample(6).join)
            @url.short_url = [*('A'..'Z'),*('0'..'9'),*('a'..'z')].sample(6).join
        end
        unless Shorturl.find_by_clean_url(@url.clean_url)  
            if @url.save
                flash[:success] = "url was successfully created"
                # redirect_to @url.clean_url
                render 'show'
            else
                render 'new'
            end
        else
            @url = Shorturl.find_by_clean_url(@url.clean_url)
            render 'show'
        end
    end
    def index
        @url = Shorturl.all
    end
    def show
        @url = Shorturl.find(params[:id])
    end
    def load
        @url = Shorturl.find_by_short_url(params[:short_url])
        redirect_to @url.clean_url
    end

    private
    def url_params
        params.require(:shorturl).permit(:raw_url)
    end
end
