class CreateShorturls < ActiveRecord::Migration[6.0]
  def change
    create_table :shorturls do |t|
      t.text :raw_url
      t.text :clean_url
      t.string :short_url
      t.timestamps
    end
  end
end
